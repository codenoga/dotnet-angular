﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 as base 
WORKDIR /app
ENV TZ 'America/Santiago'
RUN echo ${TZ} > /etc/timezone
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libgdiplus \
        libc6-dev \
        tzdata  \
    && rm /etc/localtime \
    && ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime \
    && dpkg-reconfigure -f nointeractive tzdata \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

FROM mcr.microsoft.com/dotnet/sdk:6.0 as build
EXPOSE 80
WORKDIR /app
RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs npm
COPY *.csproj .
RUN dotnet restore
COPY . .         
RUN dotnet publish -c Release -o /out

FROM base as runtime 
COPY --from=build /out . 
ENV ASPNETCORE_URLS http://*:80
ENTRYPOINT [ "dotnet", "dotnet-angular.dll"]

